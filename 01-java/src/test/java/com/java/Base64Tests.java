package com.java;

import org.junit.Test;
import java.util.Base64;

public class Base64Tests {

    @Test
    public void testEncode(){
        String key="123456";
        Base64.Encoder encoder = Base64.getEncoder();
        byte[] encode = encoder.encode(key.getBytes());
        System.out.println(new String(encode));//MTIzNDU2
    }

    @Test
    public void testDecode(){
        Base64.Decoder decoder = Base64.getDecoder();
        byte[] array = decoder.decode("MTIzNDU2");
        System.out.println(new String(array));
    }

}
