package com.java.serializable;

import com.sun.deploy.util.SyncFileAccess;

import java.io.*;

class Point implements Serializable{
    private static final long serialVersionUID = -4893006040933822688L;
    int x;
    int y;
    transient int z;
    public Point(){}
    public Point(int x,int y){
        this.x=x;
        this.y=y;
    }
    public Point(int x,int y,int z){
        this.x=x;
        this.y=y;
        this.z=z;
    }

    @Override
    public String toString() {
        return "Point{" +
                "x=" + x +
                ", y=" + y +
                ", z=" + z +
                '}';
    }
}
public class SerializableTests {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Point p1=new Point(10,20,30);
        //1.对象序列化
        ObjectOutputStream oos=new ObjectOutputStream(
                                   new FileOutputStream("point.dat"));
        oos.writeObject(p1);
        oos.close();
        //2.对象反序列化
        ObjectInputStream ois=new ObjectInputStream(new FileInputStream("point.dat"));
        Object object = ois.readObject();
        System.out.println(object);
        ois.close();

    }
}
