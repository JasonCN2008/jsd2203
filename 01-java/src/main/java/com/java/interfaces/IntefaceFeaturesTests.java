package com.java.interfaces;

interface  IA{
    default void a(){
        System.out.println("IA.a()");
    }
    default void b(){
        System.out.println("b()");
    }
    void c();
}
interface  IB{
    static void show(){
        //静态方法
    }
}

@FunctionalInterface
interface IC{
    void doSay();//函数式接口中抽象方法只能有一个
    default void doPlay(){}
    static void doWork(){}
}

class AImpl implements  IA{

    @Override
    public void a() {
        System.out.println("AImpl.a()");
    }

    @Override
    public void c() {
        System.out.println("c");
    }
}
public class IntefaceFeaturesTests {
    public static void main(String[] args) {
       IA a=new AImpl();
       a.a();
    }
}
