package com.java.interfaces;

import java.util.Arrays;
import java.util.Comparator;
import java.util.function.Consumer;

public class LambdaTests {
    static void doTest01(){
        Arrays.asList(100,200,300,400).forEach(new Consumer<Integer>() {
            @Override
            public void accept(Integer integer) {
                System.out.println(integer);
            }
        });
        Arrays.asList(100,200,300,400)
                .forEach((item)->System.out.println(item));
    }
    static void doTest02(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("thread1 is running");
            }
        }).start();
        new Thread(()-> System.out.println("thread2 is running")).start();
    }
    static void doTest03(){
        String[] strArray={"A","ABC","AB"};
        Arrays.sort(strArray, (o1, o2)->{
                return o1.length()-o2.length();
            }
        );
        System.out.println(Arrays.toString(strArray));
    }
    public static void main(String[] args) {
            //doTest01();
           //doTest02();
            doTest03();
    }
}
