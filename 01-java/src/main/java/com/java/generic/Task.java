package com.java.generic;

public interface Task <Param,Result>{
    Result execute(Param arg1);
}
