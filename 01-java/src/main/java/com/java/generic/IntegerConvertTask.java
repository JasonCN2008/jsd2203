package com.java.generic;

public class IntegerConvertTask implements Task<String,Integer>{
    @Override
    public Integer execute(String arg1) {
        return Integer.valueOf(arg1);
    }
}
