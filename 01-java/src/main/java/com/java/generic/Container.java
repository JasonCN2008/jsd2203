package com.java.generic;

public interface Container <T>{
    void add(T t);
    T get(int i);
    int size();
}
