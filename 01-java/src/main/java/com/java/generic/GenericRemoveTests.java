package com.java.generic;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * 泛型类型擦除
 */
public class GenericRemoveTests {
    public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        List<String> list=new ArrayList<>();
        list.add("A");
        list.add("B");
        //list.add(100);

        Method method =
        list.getClass().getDeclaredMethod("add", Object.class);
        method.invoke(list, 100);
        System.out.println(list);
    }
}
