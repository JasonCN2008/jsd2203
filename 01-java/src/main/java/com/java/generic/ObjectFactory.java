package com.java.generic;

import java.util.Date;

public class ObjectFactory {

    /**
     * 泛型方法，在方法的返回值左侧添加<T>,
     * FAQ?
     * 1)为什么要有方法泛型
     * 类上的泛型只能作用于类中的实例方法(方法参数，返回值类型)或非静态变量，不能作用于静态方法
     * 2)泛型方法一定是静态方法吗?不一定
     *
     * @param cls
     * @param <T>
     * @return
     * @throws IllegalAccessException
     * @throws InstantiationException
     */
    public static <T>T newInstance(Class<T> cls)
            throws IllegalAccessException, InstantiationException {
         return cls.newInstance();
    }
    public static void main(String[] args) throws InstantiationException, IllegalAccessException {
        Date o = ObjectFactory.newInstance(Date.class);
    }
}
