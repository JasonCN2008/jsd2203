package com.java.generic;

import java.util.ArrayList;
import java.util.List;

public class ContainerUtils {

    public static <T>void println(List<?> list){//无界通配符

    }
    public static <T>void printlnChar(List<? extends CharSequence> list){//上届限定通配符
        //CharSequence charSequence = list.get(0);
        //list.add("");//不允许
    }
    public static <T>void printlnNumber(List<? extends Number> list){
    }
    public static <T>void sortChar(List<? super String> list){
        Object s = list.get(0);
        System.out.println(s);
        String ss="";
        list.add(ss);
        System.out.println(list);
    }

    public static void main(String[] args) {
        List<StringBuilder> list1=new ArrayList<>();
        printlnChar(list1);

        List<Integer> list2=new ArrayList<>();
        printlnNumber(list2);

        List<String> list3=new ArrayList<>();
        list3.add("100");
        List<CharSequence> list4=new ArrayList<>();
        List<Object> list5=new ArrayList<>();

        sortChar(list3);
        //sortChar(list4);
        //sortChar(list5);
    }
}
