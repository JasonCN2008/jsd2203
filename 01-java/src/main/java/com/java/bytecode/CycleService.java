package com.java.bytecode;

/**
 * 如何使用Javassist技术对类的字节码进行修改。
 * Javassist技术是什么？
 * 是一个提供了字节码增强技术的类库。
 */
public class CycleService {
    public void doCycle(){
        System.out.println("doCycle()");
    }
}
