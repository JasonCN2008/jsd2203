package com.java.bytecode;
//对于Integer类而言，默认会有一个整数池，池中默认存储的整数为-128~+127
//也可以改变池的最大值，可以通过JVM参数进行设置：-XX:AutoBoxCacheMax=600
public class IntegerCacheTests {
    public static void main(String[] args) {
        Integer a=100;//自动封箱，会触发编译时优化，Integer.valueOf(100)
        Integer b=100;
        Integer c=200;//池大小默认是-128~+127
        Integer d=200;
        System.out.println(a==b);//true
        System.out.println(c==d);//false
    }
}
//查看类的字节码信息(javap -v 类名.class)