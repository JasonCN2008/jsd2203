package com.java.bytecode;

import java.lang.instrument.Instrumentation;

public class DefaultAgent {
    /**
     * 此方法可以在main方法启动后执行
     * @param args
     * @param instrumentation
     */
    public static void agentmain(String args, Instrumentation instrumentation){
         //添加Trabsformer对象
         instrumentation.addTransformer(new ServiceClassFileTransformer(),true);
         try {
             //设置要重新加载的类(也就是替换)
             instrumentation.retransformClasses(CycleService.class);
         }catch (Exception e){
            e.printStackTrace();
         }
    }
}
