package com.java.bytecode;

import java.lang.management.ManagementFactory;
import java.util.concurrent.TimeUnit;

/**
 * 目标JVM，这个JVM中的程序可以一直让它保持一种运行状态。
 *
 */
public class TargetJVMTests {
    public static void main(String[] args) throws InterruptedException {
        String name = ManagementFactory.getRuntimeMXBean().getName();
        //System.out.println(name);21772@DESKTOP-9A1SGJV
        String pid=name.split("@")[0];//获取jvm进程id
        System.out.println(pid);
        CycleService cycleService=new CycleService();
        while(true){
            cycleService.doCycle();
            TimeUnit.SECONDS.sleep(1);
        }
    }
}
