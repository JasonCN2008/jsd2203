package com.java.bytecode;

import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;

import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.security.ProtectionDomain;

/**
 * 通过此类中的transform方法对类的字节码进行修改，它可以调用
 * Asm或javassite技术完成字节码增强。
 */
public class ServiceClassFileTransformer implements ClassFileTransformer {
    @Override
    public byte[] transform(ClassLoader loader,
                            String className,
                            Class<?> classBeingRedefined,
                            ProtectionDomain protectionDomain,
                            byte[] classfileBuffer) throws IllegalClassFormatException {
        System.out.println(className);
        try {
            //1.获取一个ClassPool对象
            ClassPool classPool = ClassPool.getDefault();
            //2.获取一个CtClass对象
            CtClass ctClass = classPool.get("com.java.bytecode.CycleService");
            //3.获取一个CtMethod对象
            CtMethod ctMethod = ctClass.getDeclaredMethod("doCycle");
            //4.修改原有目标方法
            ctMethod.insertBefore("{System.out.println(\"start\");}");
            ctMethod.insertAfter("{System.out.println(\"end\");}");
            //5.返回类对应的字节数组
            return ctClass.toBytecode();
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }
}
