package com.java.bytecode;

import com.sun.tools.attach.AgentInitializationException;
import com.sun.tools.attach.AgentLoadException;
import com.sun.tools.attach.AttachNotSupportedException;
import com.sun.tools.attach.VirtualMachine;

import java.io.IOException;

/**
 * 基于这个JVM中的资源，去修改目标JVM中的资源
 */
public class HotReplaceJVMTests {
    public static void main(String[] args) throws IOException, AttachNotSupportedException, AgentLoadException, AgentInitializationException {
        String jar="F:\\JSDWORK\\jsd2203\\01-java\\target\\01-java-1.0-SNAPSHOT.jar";
        //链接目标JVM
        VirtualMachine targetJVM = VirtualMachine.attach("30500");//21772代表目标JVM的进程id
        //加载并运行jar包中的类
        targetJVM.loadAgent(jar);
    }
}
