package com.java.dates;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.Month;

public class LocalDateTimeTests {
    public static void main(String[] args) {
        LocalDateTime now = LocalDateTime.now();
        System.out.println(now);
        LocalDateTime ldt02 =
                LocalDateTime.of(2019, Month.DECEMBER, 31, 23, 59, 59);
        System.out.println(ldt02);//2019-12-31T23:59:59

        DayOfWeek dayOfWeek = ldt02.getDayOfWeek();
        System.out.println(dayOfWeek);

    }
}
