package com.java.dates;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DateTimeFormatterTests {
    public static void main(String[] args) {
        LocalDateTime ld4 =
                LocalDateTime.parse("2019/12/12 12:12:12",
                        DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss"));
        System.out.println(ld4);

    }
}
