package com.java.dates;

import java.time.LocalTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;

public class LocalDateTests {
    public static void main(String[] args) {
        LocalTime lt1=LocalTime.now();
        System.out.println(lt1);
        LocalTime lt2=LocalTime.now(ZoneId.systemDefault());
        System.out.println(lt2);
        long t= ChronoUnit.HOURS.between(lt1, lt2);
        System.out.println(t);

    }
}
