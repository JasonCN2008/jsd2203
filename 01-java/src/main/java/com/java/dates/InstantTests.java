package com.java.dates;

import java.time.Instant;
import java.time.ZoneId;
import java.util.concurrent.TimeUnit;

public class InstantTests {
    public static void main(String[] args) {
        //获取瞬时对象(当前时间年月日时分秒),Instant是绝对时间，没有时区的概念
        Instant instant1 =Instant.now();//Clock.systemUTC().instant();
        System.out.println(instant1);
        //通过这种方式获取的时间戳与北京时间相差8个时区，需要修正为北京时间
        instant1=instant1.plusMillis(TimeUnit.HOURS.toMillis(8));
        System.out.println(instant1);
        //输出系统可用时区
        System.out.println(ZoneId.getAvailableZoneIds());
         //输出系统默认时区
        System.out.println(ZoneId.systemDefault());
        //输出系统默认时区时间
        System.out.println(Instant.now().atZone(ZoneId.systemDefault()));
    }
}
