package com.java.param;

public class VarParamTests {
    public static void main(String[] args) {
        greets("Hello", "Peter", "Paul");
    }
    public static void greets(String greeting, String names){ }
    public static void greets(String greeting, String... names) {
        System.out.print(greeting + ",");
        // varargs received as an array
        for (int i = 0; i < names.length; ++i) {
            System.out.print(" " + names[i]);
        }
        System.out.println();
    }
}
