package com.java.stream;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class StreamTests {
    //Stream应用的三个步骤
    static void doTest01(){
        long count = Arrays.asList(1, 2, 3, 4, 5, 6, 7)
                .stream()//1.创建stream对象
                .filter((i) -> i % 2 == 0)//中间操作
                .count();//3.终止操作
        System.out.println(count);
    }
    //如何创建Stream对象
    static void doTest02(){
//         IntStream s3=Arrays.stream(new int[] {1,2,3,4});
           Stream<Integer> s4=Stream.of(10,20,30,40);
           long count = s4.filter(item -> item > 30).count();
           System.out.println(count);

//        Stream<Integer> s5=Stream.iterate(2, (x)->x+2);
//        s5.forEach(System.out::println);

//        Stream<Double> s6=Stream.generate(()->Math.random());
//        s6.forEach(System.out::println);
    }
    static void doTest03(){
        List<Integer> list=Arrays.asList(100,101,102,200);
        list.stream()
                .filter((i->i%2==0)) //获取所有偶数
                .skip(2) //跳过前两个偶数
                .forEach(System.out::println);//后面进行输出
    }
    static void doTest04(){
        List<String> list=Arrays.asList("A","BC","DEF");
        list.stream()
                .map(x->x.toUpperCase()) //将每个字符串转换为大写
                .forEach(System.out::println);

        list.stream()
                .map((x)->x.length()) //获取每个字符串的长度
                .forEach(System.out::println);

    }

    static void doTest05(){
        List<Integer> list=Arrays.asList(11,15);
        boolean flag=list.stream().allMatch((x)->x%2==0);
        System.out.println(flag);
        flag=list.stream().anyMatch((x)->x%2==0);//是否有2求余等于0的
        System.out.println(flag);
        flag=list.stream().noneMatch((x)->x>20);//是否没有大于20的
        System.out.println(flag);//false

    }

    static void doTest06() throws InterruptedException {
        List<Integer> list=Arrays.asList(11,15,16,18,19,20);
        Optional<Integer> first = list.stream().sorted().findFirst();
        System.out.println(first.get());
        while(true) {
            Optional<Integer> any =
                    list.parallelStream().filter(x->x%2!=0).findAny();
            System.out.println(any.get());
            TimeUnit.SECONDS.sleep(1);
        }

    }

    public static void main(String[] args) throws InterruptedException {
        //doTest01();
       // doTest02();
       // doTest03();
       // doTest04();
       // doTest05();
          doTest06();
    }
}
