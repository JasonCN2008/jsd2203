package com.java.methodref;

import java.util.Arrays;
import java.util.Date;
import java.util.function.Function;
import java.util.function.Supplier;

public class MethodReferenceTests {
    static void doTest01(){
        //Arrays.asList("A","B","C").forEach((item)->System.out.println(item));
        Arrays.asList("A","B","C").forEach(System.out::println);//实例方法引用
    }
    static void doTest02(){
        Supplier<Date> supplier=new Supplier<Date>() {
            @Override
            public Date get() {
                return new Date();
            }
        };

        //lambda
        //supplier=()->{return new Date();};
        supplier=Date::new;//构造方法引用
        System.out.println(supplier.get());
    }
    static void doTest03(){
        Function<String,Integer> f1=new Function<String, Integer>() {
            @Override
            public Integer apply(String s) {
                return Integer.parseInt(s);
            }
        };
        Integer n=f1.apply("100");

        f1=(str)->Integer.parseInt(str);//lambda
        n=f1.apply("200");

        f1=Integer::parseInt; //静态方法引用
        n=f1.apply("300");
        System.out.println(n);
    }
    public static void main(String[] args) {
        //doTest1();//实例方法引用，语法：对象实例::实例方法
        //doTest02();//构造方法引用，语法：类名::new
        //doTest03();//静态方法引用，语法：类名::静态方法
    }
}
