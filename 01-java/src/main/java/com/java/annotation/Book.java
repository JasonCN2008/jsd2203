package com.java.annotation;

import java.lang.reflect.Field;

@Entity
public class Book {
    @ID(column = "book_id")
    private Integer id;

    public static void main(String[] args) throws NoSuchFieldException {
        Class<Book> bookClass = Book.class;
        Field id = bookClass.getDeclaredField("id");
        ID annotation = id.getAnnotation(ID.class);
        String column = annotation.column();
        System.out.println(column);
    }
}

