package com.java.enums;

public enum Gender {
    MALE,FEMALE,NONE("没有性别要求");//这是三个枚举对象实例，类加载时创建
     Gender(){}
     private String name;
     Gender(String name){
         this.name=name;
     }

    public static void main(String[] args) {
        String name = Gender.NONE.name;
        System.out.println(name);
        //JAVA中所有的枚举类型都会默认继承Enum
        Gender[] values = Gender.values();
        for (Gender v:values){
            System.out.println(v);
        }
    }
}
