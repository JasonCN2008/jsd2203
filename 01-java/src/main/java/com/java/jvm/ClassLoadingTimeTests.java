package com.java.jvm;

/**
 * 类加载的时机？
 * 1)隐式加载(构建当前类或子类对象，访问类中属性或方法)
 * 2)显式加载(ClassLoader.loadClass(...))
 */
class ClassA{
    static int a=100;
    static{
        System.out.println("ClassA.static{}");
    }
}
class ClassB extends ClassA{
    static{
        System.out.println("ClassB.static{}");
    }
}
public class ClassLoadingTimeTests {
    public static void main(String[] args) throws ClassNotFoundException {
        ClassA c1=null;
        //Class<?> aClass =ClassLoader.getSystemClassLoader().loadClass("com.java.jvm.ClassA");

        //Class.forName("com.java.jvm.ClassA");
        //Class.forName("com.java.jvm.ClassA",//你要加载的类
               // false,//是否初始化
               // ClassLoader.getSystemClassLoader()//类加载器
               // );

        //ClassA 主动加载,ClassB被动加载
        System.out.println(ClassB.a);
    }
}
