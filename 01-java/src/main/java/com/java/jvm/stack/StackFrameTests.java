package com.java.jvm.stack;

import java.util.Collection;
import java.util.Map;

public class StackFrameTests {
    static void methodB(){
        Map<Thread, StackTraceElement[]> allStackTraces =
                Thread.getAllStackTraces();
        Collection<StackTraceElement[]> values = allStackTraces.values();
        for(StackTraceElement[] elements:values){
            for(StackTraceElement e:elements){
                System.out.println(e);
            }
        }
    }
    static void methodA(){
        methodB();
    }
    public static void main(String[] args) {
        methodA();
    }
}
