package com.java.jvm.heap;

import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class ReferenceTests {
    /**强引用*/
    static void strongReference(){
        List<byte[]> cache=new ArrayList<>();
        cache.add(new byte[1024*1024]);
        cache.add(new byte[1024*1024]);
        cache.add(new byte[1024*1024]);
        cache.add(new byte[1024*1024]);
    }
    /**软引用(此引用引用的对象生命力比强引用要弱一些，在内存不足时,对象会被销毁)*/
    static void softReference(){
        List<SoftReference> cache=new ArrayList<>();
        cache.add(new SoftReference(new byte[1024*1024]));
        cache.add(new SoftReference(new byte[1024*1024]));
        cache.add(new SoftReference(new byte[1024*1024]));
        cache.add(new SoftReference(new byte[1024*1024]));
       //使用cache.get(0).get()
    }
    /**弱引用(此引用引用的对象生命比较弱,比软引用还要弱，在触发GC时，对象会被销毁)*/
    static void weakReference(){
        List<WeakReference> cache=new ArrayList<>();
        cache.add(new WeakReference(new byte[1024*1024]));
        cache.add(new WeakReference(new byte[1024*1024]));
        cache.add(new WeakReference(new byte[1024*1024]));
        cache.add(new WeakReference(new byte[1024*1024]));
    }
    public static void main(String[] args) {
          // strongReference();
             softReference();
         //weakReference();
    }
}
