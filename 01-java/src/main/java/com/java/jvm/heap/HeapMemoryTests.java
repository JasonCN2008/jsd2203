package com.java.jvm.heap;

//-Xmx6m -Xms6m
public class HeapMemoryTests {
    public static void main(String[] args) {
        //大对象导致堆内存溢出
        //byte[]array=new byte[1024*1024*6];

        //频繁创建对象导致内存溢出
        byte[] d1=new byte[1024*1024];
        byte[] d2=new byte[1024*1024];
        byte[] d3=new byte[1024*1024];
        byte[] d4=new byte[1024*1024];
        byte[] d5=new byte[1024*1024];
    }
}
