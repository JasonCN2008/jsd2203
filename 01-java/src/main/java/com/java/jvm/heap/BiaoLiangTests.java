package com.java.jvm.heap;
//JVM参数
//-Xmx用于设置最大堆大小
//-Xms用于设置初始堆大小
//-XX:+EliminateAllocations 用于标量替换
//-XX:+PrintGC输出gc信息
//-Xmx128m -Xms128m -XX:+DoEscapeAnalysis -XX:+EliminateAllocations  -XX:+PrintGC

public class BiaoLiangTests {
    public static void main(String[] args) {
        long t1=System.nanoTime();
        for(int i=0;i<100000000;i++){
            alloc();
        }
        System.out.println("time="+(System.nanoTime()-t1));
    }

    static void alloc(){
        Point p1=new Point(10,20);
       //标量替换(相对于将上面的对象打散，把变量直接存储到栈上)
       //int x=10;
       //int y=20;
    }
}
class Point{
    int x;
    int y;
    public Point(int x,int y){
        this.x=x;
        this.y=y;
    }
}
