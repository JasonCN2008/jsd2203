package com.java.jvm.heap;
class Outer{
    static class Inner{//静态内部类默认是不会保存外部类引用的
        public void run(){}
    }
    @Override
    protected void finalize() throws Throwable {
        System.out.println(Thread.currentThread().getName()+"->finalize()");
    }
}
public class OuterInnerTests {
    public static void main(String[] args) throws InterruptedException {
         Outer outer=new Outer();
         Outer.Inner inner= new Outer.Inner();
         outer=null;
         //手动启动GC
         System.gc();//(底层是工作线程执行GC操作)

    }
}
