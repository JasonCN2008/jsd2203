package com.java.jvm;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

/**
 * 假如需要从指定磁盘中加载某个类可以使用UrlClassLoader
 */
public class UrlClassLoaderTests {
    public static void main(String[] args) throws ClassNotFoundException, MalformedURLException {
        //1.创建要加载的文件目录对象
        String classFileDir="F:\\JSDWORK";
        File file=new File(classFileDir);
        //2.获取目录对应的url
        URL[] urlArray={file.toURI().toURL()};

        //3.使用UrlClassLoader加载类
        URLClassLoader classLoader1=
                new URLClassLoader(urlArray,ClassLoader.getSystemClassLoader());
        Class<?> aClass1 = classLoader1.loadClass("pkg.Hello");

        URLClassLoader classLoader2=
                new URLClassLoader(urlArray,ClassLoader.getSystemClassLoader());

        Class<?> aClass2 = classLoader2.loadClass("pkg.Hello");
        System.out.println(aClass1==aClass2);

    }
}
