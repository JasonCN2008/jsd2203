package com.java.jvm;

/**
 * 类加载时会先加载父类再加载子类
 * JVM参数分析
 * -XX:+TraceClassLoading 跟踪类的加载
 * 类加载时默认支持懒加载(类不使用，暂时不会读取到内存)
 */
public class ClassTraceTests {
    public static void main(String[] args) {
        System.out.println("Class Trace");
    }
}
