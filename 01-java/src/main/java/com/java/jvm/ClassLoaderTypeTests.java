package com.java.jvm;

/**
 * 获取类的类加载器
 */
public class ClassLoaderTypeTests {
    public static void main(String[] args) {
        //1.获取当前类的类加载器(AppClassLoader)
        ClassLoader systemClassLoader =
                ClassLoader.getSystemClassLoader();//AppClassLoader
        System.out.println(systemClassLoader);
        //2.获取扩展类的类加载器(ExtClassLoader)
        ClassLoader extClassLoader = systemClassLoader.getParent();
        System.out.println(extClassLoader);//ExtClassLoader
        //3.获取根类加载器(BootStrapClassLoader)
        ClassLoader parent = extClassLoader.getParent();
        System.out.println(parent);//null
    }
}
