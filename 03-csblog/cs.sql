-- MySQL dump 10.13  Distrib 5.7.21, for Win64 (x86_64)
--
-- Host: localhost    Database: cs
-- ------------------------------------------------------
-- Server version	5.7.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping database structure for cs
DROP DATABASE IF  EXISTS `cs`;
CREATE DATABASE IF NOT EXISTS `cs` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `cs`;

--
-- Table structure for table `banner`
--

DROP TABLE IF EXISTS `banner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banner`
--

LOCK TABLES `banner` WRITE;
/*!40000 ALTER TABLE `banner` DISABLE KEYS */;
INSERT INTO `banner` VALUES (2,'/imgs/b2.jpg'),(5,'/ec65deeb-f27a-4056-9fbd-45e1fdad1695.jpg'),(6,'/cb3b50b1-9d55-4ecd-ba1e-d769f373fd39.png'),(7,'/a01a6b5c-189a-494a-a956-027bf31b0dd6.jpg'),(8,'/804d2827-7b03-4387-a6dc-e71040fe94f0.jpg'),(9,'/70f9a0a8-c4ca-4587-86f0-4d1b95b0bf74.jpg'),(11,'/ed9da81a-4e15-49a1-b760-2656921afb21.png'),(17,'/076ccb9f-dab3-4ee1-b724-e2f3b4faee83.jpg');
/*!40000 ALTER TABLE `banner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,'女装'),(2,'男装'),(3,'医药'),(11,'书籍');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log`
--

DROP TABLE IF EXISTS `log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL COMMENT '用户名',
  `ip` varchar(64) DEFAULT NULL COMMENT 'IP地址',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `operation` varchar(50) DEFAULT NULL COMMENT '用户操作',
  `method` varchar(200) DEFAULT NULL COMMENT '请求方法',
  `params` varchar(5000) DEFAULT NULL COMMENT '请求参数',
  `time` bigint(20) NOT NULL COMMENT '执行时长(毫秒)',
  `status` int(11) DEFAULT '1',
  `error` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8 COMMENT='用户日志';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log`
--

LOCK TABLES `log` WRITE;
/*!40000 ALTER TABLE `log` DISABLE KEYS */;
INSERT INTO `log` VALUES (1,NULL,'浏览具体商品','com.blog.controller.ProductController.selectById','[3]',457,'127.0.0.1',NULL,1,''),(2,NULL,'浏览具体商品','com.blog.controller.ProductController.selectById','[3]',64,'127.0.0.1',NULL,1,''),(3,NULL,'浏览具体商品','com.blog.controller.ProductController.selectById','[5]',72,'127.0.0.1',NULL,1,''),(4,NULL,'浏览具体商品','com.blog.controller.ProductController.selectById','[7]',79,'127.0.0.1',NULL,1,''),(5,NULL,'浏览具体商品','com.blog.controller.ProductController.selectById','[10]',72,'127.0.0.1',NULL,1,''),(6,NULL,'浏览具体商品','com.blog.controller.ProductController.selectById','[9]',71,'127.0.0.1',NULL,1,''),(7,NULL,'浏览具体商品','com.blog.controller.ProductController.selectById','[3]',2376,'0:0:0:0:0:0:0:1','2022-06-30 14:25:00',1,''),(8,NULL,'浏览具体商品','com.blog.controller.ProductController.selectById','[5]',75,'0:0:0:0:0:0:0:1','2022-06-30 14:25:01',1,''),(9,NULL,'浏览具体商品','com.blog.controller.ProductController.selectById','[7]',83,'0:0:0:0:0:0:0:1','2022-06-30 14:25:05',1,''),(10,NULL,'浏览具体商品','com.blog.controller.ProductController.selectById','[8]',67,'0:0:0:0:0:0:0:1','2022-06-30 14:25:09',1,''),(11,NULL,'浏览具体商品','com.blog.controller.ProductController.selectById','[10]',52,'0:0:0:0:0:0:0:1','2022-06-30 14:25:13',1,''),(12,NULL,'浏览具体商品','com.blog.controller.ProductController.selectById','[11]',72,'0:0:0:0:0:0:0:1','2022-06-30 14:25:18',1,''),(13,NULL,'浏览具体商品','com.blog.controller.ProductController.selectById','[4]',450,'0:0:0:0:0:0:0:1','2022-06-30 14:25:51',1,''),(14,NULL,'浏览具体商品','com.blog.controller.ProductController.selectById','[4]',157,'0:0:0:0:0:0:0:1','2022-06-30 14:26:01',1,''),(15,NULL,'浏览具体商品','com.blog.controller.ProductController.selectById','[4]',74,'0:0:0:0:0:0:0:1','2022-06-30 14:26:10',1,''),(16,NULL,'浏览具体商品','com.blog.controller.ProductController.selectById','[5]',68,'0:0:0:0:0:0:0:1','2022-06-30 14:26:14',1,''),(17,NULL,'浏览具体商品','com.blog.controller.ProductController.selectById','[4]',124,'0:0:0:0:0:0:0:1','2022-06-30 14:26:18',1,''),(18,NULL,'浏览具体商品','com.blog.controller.ProductController.selectById','[8]',67,'0:0:0:0:0:0:0:1','2022-06-30 14:28:09',1,''),(19,NULL,'浏览具体商品','com.blog.controller.ProductController.selectById','[8]',66,'0:0:0:0:0:0:0:1','2022-06-30 14:28:33',1,''),(20,'user','浏览具体商品','com.blog.controller.ProductController.selectById','[3]',213,'0:0:0:0:0:0:0:1','2022-07-03 09:11:50',1,''),(21,'user','浏览具体商品','com.blog.controller.ProductController.selectById','[5]',470,'0:0:0:0:0:0:0:1','2022-07-03 09:12:30',1,''),(22,'user','浏览具体商品','com.blog.controller.ProductController.selectById','[3]',539,'0:0:0:0:0:0:0:1','2022-07-08 19:17:38',1,''),(23,'user','浏览具体商品','com.blog.controller.ProductController.selectById','[4]',37304,'0:0:0:0:0:0:0:1','2022-07-08 20:17:24',1,''),(24,'','浏览具体商品','com.blog.controller.ProductController.selectById','[9]',477,'0:0:0:0:0:0:0:1','2022-07-10 10:43:33',1,''),(25,'','浏览具体商品','com.blog.controller.ProductController.selectById','[9]',460,'192.168.3.5','2022-07-10 10:44:58',1,''),(26,'','浏览具体商品','com.blog.controller.ProductController.selectById','[5]',72,'192.168.3.5','2022-07-10 10:45:03',1,''),(27,'','浏览具体商品','com.blog.controller.ProductController.selectById','[4]',862006,'192.168.3.5','2022-07-10 11:47:59',1,''),(28,'','浏览具体商品','org.springframework.aop.aspectj.MethodInvocationProceedingJoinPoint.selectById','[8]',2383,'192.168.3.5','2022-07-10 11:49:08',1,''),(29,'','浏览具体商品','com.blog.controller.ProductController.selectById','[4]',95,'192.168.3.5','2022-07-10 14:52:28',1,''),(30,'','浏览具体商品','com.blog.controller.ProductController.selectById','[8]',76,'192.168.3.5','2022-07-10 15:21:21',1,''),(31,'','浏览具体商品','com.blog.controller.ProductController.selectById','[10]',74,'192.168.3.5','2022-07-10 15:21:24',1,''),(32,'','浏览具体商品','com.blog.controller.ProductController.selectById','[10]',203,'192.168.3.5','2022-07-10 15:21:29',1,''),(33,'','浏览具体商品','com.blog.controller.ProductController.selectById','[10]',597,'192.168.3.5','2022-07-10 15:23:09',1,''),(34,'','浏览具体商品','com.blog.controller.ProductController.selectById','[7]',33,'192.168.3.5','2022-07-10 15:23:17',1,''),(35,'','浏览具体商品','com.blog.controller.ProductController.selectById','[11]',73,'192.168.3.5','2022-07-10 15:23:40',1,''),(36,'','浏览具体商品','com.blog.controller.ProductController.selectById','[9]',86,'192.168.3.5','2022-07-10 15:25:19',1,''),(37,'','浏览具体商品','com.blog.controller.ProductController.selectById','[8]',71,'192.168.3.5','2022-07-10 15:25:23',1,''),(38,'','浏览具体商品','com.blog.controller.ProductController.selectById','[10]',56,'192.168.3.5','2022-07-10 15:25:26',1,''),(39,'','浏览具体商品','com.blog.controller.ProductController.selectById','[4]',152,'192.168.3.5','2022-07-10 15:46:45',1,''),(40,'','浏览具体商品','com.blog.controller.ProductController.selectById','[5]',96,'192.168.3.5','2022-07-10 15:52:03',1,''),(41,'','查询分类列表','com.blog.controller.CategoryController.list','[]',538,'192.168.3.5','2022-07-10 17:44:11',1,''),(42,'','查询分类列表','com.blog.controller.CategoryController.list','[]',10,'192.168.3.5','2022-07-10 17:44:13',1,''),(43,'admin','查询分类列表','com.blog.controller.CategoryController.list','[]',1,'0:0:0:0:0:0:0:1','2022-07-10 17:44:39',1,''),(44,'admin','查询分类列表','com.blog.controller.CategoryController.list','[]',5,'0:0:0:0:0:0:0:1','2022-07-10 17:44:58',1,''),(45,'','查询分类列表','com.blog.controller.CategoryController.list','[]',0,'192.168.3.5','2022-07-10 17:45:09',1,''),(46,'user','浏览具体商品','com.blog.controller.ProductController.selectById','[3]',85,'0:0:0:0:0:0:0:1','2022-07-11 08:39:10',1,''),(47,'','查看商品详情','com.blog.controller.ProductController.doSelectById','[4]',8,'0:0:0:0:0:0:0:1','2022-07-19 16:59:52',1,''),(48,'','查看商品详情','com.blog.controller.ProductController.doSelectById','[7]',11,'0:0:0:0:0:0:0:1','2022-07-19 17:00:11',1,''),(49,'admin','login','com.blog.controller.UserController.doLogin','[{\"id\":null,\"username\":\"admin\",\"password\":\"123456\",\"nick\":null}]',522,'0:0:0:0:0:0:0:1','2022-07-19 17:04:26',1,''),(50,'admin','login','com.blog.controller.UserController.doLogin','[{\"id\":null,\"username\":\"admin\",\"password\":\"123456\",\"nick\":null}]',451,'0:0:0:0:0:0:0:1','2022-07-19 17:10:38',1,''),(51,'admin','分页查询商品信息','com.blog.controller.ProductController.doSelectAdmin','[1]',141,'0:0:0:0:0:0:0:1','2022-07-19 17:10:39',1,''),(52,'admin','分页查询商品信息','com.blog.controller.ProductController.doSelectAdmin','[2]',12,'0:0:0:0:0:0:0:1','2022-07-19 17:11:06',1,''),(53,'admin','分页查询商品信息','com.blog.controller.ProductController.doSelectAdmin','[3]',8,'0:0:0:0:0:0:0:1','2022-07-19 17:11:08',1,''),(54,'admin','分页查询商品信息','com.blog.controller.ProductController.doSelectAdmin','[1]',324,'0:0:0:0:0:0:0:1','2022-07-21 16:26:23',1,''),(55,'','查看商品详情','com.blog.controller.ProductController.doSelectById','[3]',19,'0:0:0:0:0:0:0:1','2022-08-03 20:05:12',1,''),(56,'','查看商品详情','com.blog.controller.ProductController.doSelectById','[4]',4,'0:0:0:0:0:0:0:1','2022-08-03 20:05:30',1,''),(57,'','查看商品详情','com.blog.controller.ProductController.doSelectById','[4]',26,'0:0:0:0:0:0:0:1','2022-08-09 11:08:36',1,'');
/*!40000 ALTER TABLE `log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `url` varchar(100) DEFAULT NULL,
  `price` double(10,2) DEFAULT NULL,
  `old_price` double(10,2) DEFAULT NULL,
  `view_count` int(11) DEFAULT NULL,
  `sale_count` int(11) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (3,'森马牛仔裤女宽松慢跑裤运动风2022春季新款显瘦束脚长裤复古','/8f57e641-1f9c-4393-8a36-b9f0d2c16ef2.jpg',654.00,3453.00,30,334,'2022-07-11 00:39:10',2),(4,'茵曼马甲连衣裙两件套春季新款娃娃领色织格长袖背心裙套装','/5080711d-52bb-4e57-a645-1674857fd1f2.jpg',123.00,234.00,13,465,'2022-07-10 07:46:45',2),(5,'雪中飞墨绿色短袖t恤女夏2022新款纯棉半袖打底体恤夏季上衣潮ins','/734de500-e2ce-4e98-93d9-edadf2e20bf6.jpg',33.00,432.00,10,23,'2022-07-10 07:52:02',1),(7,'弓箭','/24bcbe4d-894d-4766-b8b2-20faf533a5cd.jpg',5.00,7.00,5,2,'2022-07-10 07:23:17',8),(8,'香影毛呢外套女中长款2021年冬季新款气质韩版娃娃领紫色呢子大衣','/f1a9da0b-bd98-4586-b029-9a77a8ed993b.jpg',343.00,423.00,10,234,'2022-07-10 07:25:22',1),(9,'非常新鲜的食品','/8b0c64cc-2987-4faf-80f1-9eddfc4400de.jpg',50.00,100.00,7,5,'2022-07-10 07:25:18',9),(10,'JANE HARLOW男鞋运动鞋网面防臭老爹鞋韩版潮流学生百搭休闲鞋','/6a3c5ce4-4a8b-410b-8b00-5b6e60e643ef.jpg',22.00,33.00,6,443,'2022-07-10 07:25:26',3),(11,'JANE HARLOW男鞋运动鞋网面防臭老爹鞋韩版潮流学生百搭休闲鞋','/22034b65-675f-4f65-8b02-2f5bfe235c28.jpg',33.00,44.00,3,234,'2022-07-10 07:23:39',8);
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `nick` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'admin','e10adc3949ba59abbe56e057f20f883e','管理员');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-08-19 11:09:36
