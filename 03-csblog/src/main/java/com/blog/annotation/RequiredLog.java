package com.blog.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 自定义注解,通过此注解去描述方法,将被描述的方法作为切入点方法.
 * @Retention 用于描述你定义的注解何时有效.
 * @Target注解用于描述你定义的注解可以描述哪些对象(类,属性,方法,...)
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface RequiredLog {//public interface RequiredLog extends Annotation{}
    /**定义一个抽象方法,默认返回为空串*/
    String operation() default "";
}
//如何知道这个注解本质上就是一个接口的?
//在当前类所在的路径执行javap -v  RequiredLog.class