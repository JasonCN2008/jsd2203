package com.blog.service;

import com.blog.pojo.Log;

/**
 * 日志业务接口
 */
public interface LogService {
    void insert(Log log);
}
