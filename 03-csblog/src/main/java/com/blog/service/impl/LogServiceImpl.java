package com.blog.service.impl;

import com.blog.mapper.LogMapper;
import com.blog.pojo.Log;
import com.blog.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class LogServiceImpl implements LogService {
    @Autowired
    private LogMapper logMapper;

    /**
     * @Async 注解描述的方法为异步切入点方法,底层会通过AOP方法将此方法运行在一个新的线程中.
     * @param log
     */
    @Async
    @Override
    public void insert(Log log) {
//        new Thread(){//1M
//            @Override
//            public void run() {
//                String tName=Thread.currentThread().getName();
//                System.out.println("LogServiceImpl.thread.name="+tName);
//                try{Thread.sleep(5000);}catch (Exception e){}
//                logMapper.insert(log);
//            }
//        }.start();

         String tName=Thread.currentThread().getName();
         System.out.println("LogServiceImpl.thread.name="+tName);
         try{Thread.sleep(5000);}catch (Exception e){}
         logMapper.insert(log);
    }
}
