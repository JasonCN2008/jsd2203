package com.spring.dao;

import org.apache.ibatis.session.SqlSession;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

@SpringBootTest
public class ConnectionTests {

    /**
     * DataSource是所有连接池必须要实现的一个接口，
     * 是JDBC中的一个规范。
     */
    @Autowired
    private DataSource dataSource;

    @Autowired
    private SqlSession sqlSession;

    @Test
    void testGetConnection01() throws SQLException {
        Connection connection =
                dataSource.getConnection();
        System.out.println(connection);
    }
    @Test
    void testGetConnection02() throws SQLException {
        Connection connection =
                sqlSession.getConnection();//SqlSession获取连接时还是通过DataSource对象去获取的
        System.out.println(connection);
    }


}
