package com.spring.dao;

import org.apache.ibatis.session.SqlSession;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.PrintStream;
import java.util.Arrays;
import java.util.List;

@SpringBootTest
public class SqlSessionTests {

    /**
     * SqlSession是MyBatis与数据库进行会话的核心对象API对象
     */
     @Autowired
     private SqlSession sqlSession;

     @Test
     void selectTags(){
         String statment="com.spring.dao.TagDao.list";
         List<Object> objects =
                 sqlSession.selectList(statment);
         //objects.forEach((item)-> System.out.println());//lambda
         objects.forEach(System.out::println); //lambda
         for(Object o:objects){
             System.out.println(o);
         }
     }
}
